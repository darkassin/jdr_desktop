﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppJdr
{
    /// <summary>
    /// Logique d'interaction pour CreateJDR.xaml
    /// </summary>
    public partial class CreateJDR : Window
    {
        public CreateJDR()
        {
            InitializeComponent();
        }
        private void Back_Main(object sender, RoutedEventArgs e)
        {
            this.Hide();
            MainWindow mainP = new MainWindow();
            mainP.Show();
        }
    }
}
